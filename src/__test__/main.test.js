/**
 * @jest-environment jsdom
 */

async function setUpDomAndLoadMain() {
  document.body.innerHTML = `
    <button id="add-next-x-values">x-Wert hinzufügen</button>
    <button id="remove-last-x-values">x-Wert wegnehmen</button>
    <table id="function-values">
      <tbody>
        <tr>
          <td>0</td><td>-3</td><td>-3</td><td>1</td>
        </tr>
        <tr>
          <td>1</td><td>-1</td><td>1</td><td>2</td>
        </tr>
        <tr>
          <td>2</td><td>1</td><td>7</td><td>4</td>
        </tr>
        <tr>
          <td></td>
          <td>
            <button class="toggle-bg-color">Farbe hinzu</button>
          </td>
          <td>
            <button class="toggle-bg-color">Farbe hinzu</button>
          </td>
          <td>
            <button class="toggle-bg-color">Farbe hinzu</button>
          </td>
        </tr>
      </tbody>
    </table>`;
  await import('../scripts/main');
}

describe('function values are correctly added and removed', () => {
  beforeEach(async () => {
    await setUpDomAndLoadMain();
  });

  afterEach(() => {
    jest.resetModules();
  });

  function checkTableValues(expectedValues) {
    // Zeilen aus Tabelle in DOM auslesen
    const elRows = document.querySelectorAll('#function-values tbody tr');
    // Ist Anzahl Zeilen korrekt?
    expect(elRows.length - 1).toBe(expectedValues.length);
    for (let i = 0; i < elRows.length - 1; i++) {
      const elRow = elRows[i];
      const expectedRow = expectedValues[i];
      const elCells = elRow.getElementsByTagName('td');
      // Ist Anzahl Werte in Zeile korrekt?
      expect(elCells.length).toBe(expectedRow.length);
      for (let j = 0; j < elCells.length; j++) {
        const elCell = elCells[j];
        const actualValue = parseInt(elCell.textContent);
        const expectedValue = expectedRow[j];
        // Ist Funktionswert korrekt?
        expect(actualValue).toBe(expectedValue);
      }
    }
  }

  test('adding values correctly', () => {
    // given
    // when - click button to add values
    const elAddBtn = document.getElementById('add-next-x-values');
    elAddBtn.click();
    // then
    checkTableValues([
      [0, -3, -3, 1],
      [1, -1, 1, 2],
      [2, 1, 7, 4],
      [3, 3, 15, 8],
    ]);
  });

  test('removing values correctly', () => {
    // given
    // when - click button to remove values
    const elRemoveBtn = document.getElementById('remove-last-x-values');
    elRemoveBtn.click();
    // then
    checkTableValues([
      [0, -3, -3, 1],
      [1, -1, 1, 2],
    ]);
  });
});

describe('function values in column are highlighted correctly', () => {
  beforeEach(async () => {
    await setUpDomAndLoadMain();
  });

  afterEach(() => {
    jest.resetModules();
  });

  function checkBgColorClassInColumn(colPos, expectedIsPresent) {
    // Zellen von Spalte in DOM auslesen
    const elColumnCells = document.querySelectorAll(
      `#function-values tbody tr td:nth-child(${colPos})`
    );
    // Überprüfen ob Attribut vorhanden
    for (let i = 0; i < elColumnCells.length - 1; i++) {
      const elColumnCell = elColumnCells[i];
      const isClassPresent = elColumnCell.classList.contains('bg-color');
      expect(isClassPresent).toBe(expectedIsPresent);
    }
  }

  test('column is highlighted correctly', () => {
    // given
    // when - click button to highlight column for 1st function
    const elAddBgColorBtns = document.querySelectorAll(
      'tbody .toggle-bg-color'
    );
    elAddBgColorBtns[0].click();
    // then
    checkBgColorClassInColumn(2, true);
  });

  test('column highlighting is removed correctly', () => {
    // given - click button to highlight column for 2nd function
    const elAddBgColorBtns = document.querySelectorAll(
      'tbody .toggle-bg-color'
    );
    elAddBgColorBtns[1].click();
    // when - click button to highlight column for 2nd function
    elAddBgColorBtns[1].click();
    // then
    checkBgColorClassInColumn(3, false);
  });
});
