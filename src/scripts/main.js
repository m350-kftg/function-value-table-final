function toggleBgColor(elCellsToColor) {
  for (let idx = 0; idx < elCellsToColor.length - 1; idx++) {
    const elCellToColor = elCellsToColor[idx];
    elCellToColor.classList.toggle('bg-color');
  }
}

function getCallbackToAddBgColor(colPos) {
  let addBgColor = (e) => {
    // toggle class-Attribut bg-color
    const elCellsToColor = document.querySelectorAll(`td:nth-child(${colPos})`);
    toggleBgColor(elCellsToColor);
    // Button anpassen, um die Hintergrunfarbe wieder entfernen zu können
    const elBtn = e.target;
    elBtn.textContent = 'Farbe weg';
    elBtn.removeEventListener('click', addBgColor);
    elBtn.addEventListener('click', getCallbackToRemoveBgColor(colPos));
  };
  return addBgColor;
}

function getCallbackToRemoveBgColor(colPos) {
  let removeBgColor = (e) => {
    // toggle class-Attribut bg-color
    const elCellsToColor = document.querySelectorAll(`td:nth-child(${colPos})`);
    toggleBgColor(elCellsToColor);
    // Button anpassen, um die Hintergrunfarbe hinzufügen zu können
    const elBtn = e.target;
    elBtn.textContent = 'Farbe hinzu';
    elBtn.removeEventListener('click', removeBgColor);
    elBtn.addEventListener('click', getCallbackToAddBgColor(colPos));
  };
  return removeBgColor;
}

function addXValue() {
  // Zweitletzte Zeile kopieren
  const elRowToCopy = document.querySelector('tbody tr:nth-last-child(2)');
  const elRowClone = elRowToCopy.cloneNode(true);
  const el1stTd = elRowClone.querySelector('td');
  // Hinzuzufügenden x-Wert ermitteln
  const newXValue = parseInt(el1stTd.textContent) + 1;
  if (newXValue <= 10) {
    // Neue Funktionswerte berechnen und td-Element als Inhalt hinzufügen
    el1stTd.textContent = newXValue;
    const el2ndTd = elRowClone.querySelector('td:nth-child(2)');
    el2ndTd.textContent = newXValue * 2 - 3;
    const el3rdTd = elRowClone.querySelector('td:nth-child(3)');
    el3rdTd.textContent = newXValue * newXValue + 3 * newXValue - 3;
    const el4thTd = elRowClone.querySelector('td:nth-child(4)');
    el4thTd.textContent = 2 ** newXValue;
    // Kopiertes Element vor letzte Zeile hinzufügen
    const elLastRow = document.querySelector('tbody tr:last-child');
    const elTBody = document.querySelector('tbody');
    elTBody.insertBefore(elRowClone, elLastRow);
  }
}

function removeXValue() {
  // Zweitletzte Zeile kopieren
  const elRowToRemove = document.querySelector('tbody tr:nth-last-child(2)');
  const xValue = parseInt(
    elRowToRemove.querySelector('td:first-child').textContent
  );
  if (xValue > 0) {
    // Zweitletze Zeile entfernen, sofern es noch Zeilen gibt
    elRowToRemove.remove();
  }
}

// Event Listeners für Buttons hinzufügen
// Buttons für Hinzufügen / Entfernen von x-Werten
const elBtnToAdd = document.getElementById('add-next-x-values');
elBtnToAdd.addEventListener('click', addXValue);
const elBtnToRemove = document.getElementById('remove-last-x-values');
elBtnToRemove.addEventListener('click', removeXValue);
// Buttons für farbliches Hervorheben von Spalten
const elBtnsBgColor = document.querySelectorAll('tbody .toggle-bg-color');
for (let i = 0; i < elBtnsBgColor.length; i++) {
  const elBtnBgColor = elBtnsBgColor[i];
  elBtnBgColor.addEventListener('click', getCallbackToAddBgColor(i + 2));
}
